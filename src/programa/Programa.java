package programa;

import java.time.LocalDate;

import java.util.Scanner;

import clases.Ayuntamiento;
import clases.Empleado;
import clases.Monumento;

/**
 * Clase Programa que permite al usuario interactuar con la aplicaci�n mediante
 * un men�. Los datos necesarios se introducen por consola.
 * 
 * @author Klara Galuskova, 1� DAW
 */
public class Programa {

	static Scanner input = new Scanner(System.in);
	static Ayuntamiento zaragoza = new Ayuntamiento();

	public static void main(String[] args) {

		// 1- Crear instancia de GestorClases.

		String opcion;

		do {
			System.out.println("__________________________");
			System.out.println("          MEN�");
			System.out.println("__________________________");
			System.out.println("Elige una opci�n del men�:");
			System.out.println("1 - Alta empleado");
			System.out.println("2 - Listar empleados");
			System.out.println("3 - Buscar empleado por dni");
			System.out.println("4 - Eliminar empleado");
			System.out.println("5 - Alta monumento");
			System.out.println("6 - Listar monumentos");
			System.out.println("7 - Buscar monumento por c�digo");
			System.out.println("8 - Eliminar monumento");
			System.out.println("9 - Listar monumentos por estilo");
			System.out.println("10 - Listar monumentos d�nde trabaja el empleado");
			System.out.println("11 - Asignar empleado al monumento");
			System.out.println("12 - Asignar responsable al monumento por mayor antig�edad");
			System.out.println("13 - Asignar responsable al monumento por dni");
			System.out.println("14 - A�adir fecha fin de contrato de un empleado");
			System.out.println("15 - Eliminar empleados despedidos de un monumento");
			System.out.println("16 - Sumar IVA al precio del monumento y mostrarlo");
			System.out.println("Para salir introducir 'FIN'/'fin'");
			opcion = input.nextLine().toLowerCase();

			switch (opcion) {

			case "1":
				pedirDatosEmpleado();
				break;

			case "2":
				zaragoza.listarEmpleados();
				break;

			case "3":

				if (hayEmpleados()) {
					String dni = pedirDniEmpleado();
					Empleado empleado = zaragoza.buscarEmpleado(dni);
					mostrarMensajeBuscarEmpleado(dni, empleado);
				}

				break;

			case "4":

				if (hayEmpleados()) {
					String dni = pedirDniEmpleado();
					boolean eliminado = zaragoza.eliminarEmpleado(dni);
					mostrarMensajeEliminarEmpleado(dni, eliminado);
				}

				break;

			case "5":
				pedirDatosMonumento();
				break;

			case "6":
				zaragoza.listarMonumentos();
				break;

			case "7":

				if (hayMonumentos()) {
					String codigo = pedirCodigoMonumento();
					Monumento monumento = zaragoza.buscarMonumento(codigo);
					mostrarMensajeBuscarMonumento(codigo, monumento);
				}

				break;

			case "8":

				if (hayMonumentos()) {
					String codigo = pedirCodigoMonumento();
					boolean eliminado = zaragoza.eliminarMonumento(codigo);
					mostrarMensajeEliminarMonumento(codigo, eliminado);
				}

				break;

			case "9":
				if (hayMonumentos()) {
					zaragoza.listarMonumentosPorEstilo(pedirEstiloMonumento());
				}
				break;

			case "10":
				// 10 - Listar monumentos d�nde trabaja el empleado
				if (hayMonumentos() && hayEmpleados()) {
					zaragoza.listarMonumentosEmpleado(pedirDniEmpleado());
				}

				break;

			case "11":
				// 11 - Asignar empleado al monumento
				if (hayMonumentos() && hayEmpleados()) {
					zaragoza.asignarEmpleadoMonumento(pedirCodigoMonumento(), pedirDniEmpleado());
				}

				break;

			case "12":
				// 12 - Asignar responsable al monumento por mayor antig�edad
				if (hayMonumentos() && hayEmpleados()) {
					zaragoza.asignarResponsableMonumento(pedirCodigoMonumento());
				}

				break;

			case "13":
				// 13 - Asignar responsable al monumento por dni
				if (hayMonumentos() && hayEmpleados()) {
					zaragoza.asignarResponsableMonumento(pedirCodigoMonumento(), pedirDniEmpleado());
				}

				break;

			case "14":
				// 14 - A�adir fecha fin de contrato de un empleado
				if (hayEmpleados()) {
					zaragoza.anadirFinContratoEmpleado(pedirDniEmpleado(), pedirFechaFinContratoEmpleado());
				}

				break;

			case "15":
				// 14 - Eliminar empleados despedidos de un monumento
				if (hayMonumentos() && hayEmpleados()) {
					zaragoza.bajaEmpleados(pedirCodigoMonumento());
				}

				break;

			case "16":
				// 15 - Sumar IVA al precio del monumento y mostrarlo
				if (hayMonumentos()) {
					zaragoza.sumarIvaAlPrecio(pedirCodigoMonumento());
				}

				break;

			case "fin":
				terminarPrograma();
				break;

			default:
				System.out.println("La opci�n '" + opcion + "' no es v�lida");
				break;

			}

		} while (!opcion.equals("fin"));

	}

	private static void pedirDatosMonumento() {

		boolean masDatos;
		int counter = 0;

		do {

			counter++;

			System.out.println("Monumento " + counter + " - Introduce el nombre: ");
			String nombre = input.nextLine();
			System.out.println("Monumento " + counter + " - Introduce el c�digo: ");
			String codigo = input.nextLine();
			System.out.println("Monumento " + counter + " - Introduce el estilo: ");
			String estilo = input.nextLine();
			System.out.println("Monumento " + counter + " - Introduce el precio: ");
			double precio = input.nextDouble();

			zaragoza.altaMonumento(nombre, codigo, estilo, precio);

			input.nextLine();

			masDatos = introducirMasDatos("monumento");

		} while (masDatos == true);

	}

	private static String pedirCodigoMonumento() {

		System.out.println("Introduce el c�digo del monumento:");
		String codigo = input.nextLine();

		return codigo;

	}

	private static String pedirEstiloMonumento() {

		System.out.println("Introduce el estilo del monumento:");
		String estilo = input.nextLine();

		return estilo;

	}

	public static void pedirDatosEmpleado() {

		boolean masDatos;
		int counter = 0;

		do {

			counter++;

			System.out.println("Empleado " + counter + " - Introduce el nombre: ");
			String nombre = input.nextLine();
			System.out.println("Empleado " + counter + " - Introduce los apellidos: ");
			String apellidos = input.nextLine();
			System.out.println("Empleado " + counter + " - Introduce el dni: ");
			String dni = input.nextLine();
			System.out.println("Empleado " + counter
					+ " - Introduce la fecha de nacimiento: (a�o-mes-d�a, en formato YYYY-MM-DD)");
			String fechaNacimiento = input.nextLine();
			System.out.println("Empleado " + counter
					+ " - Introduce la fecha de inicio de contrato: (a�o-mes-d�a, en formato YYYY-MM-DD)");
			String inicioContrato = input.nextLine();

			zaragoza.altaEmpleado(nombre, apellidos, dni, fechaNacimiento, inicioContrato);

			masDatos = introducirMasDatos("empleado");

		} while (masDatos == true);
	}

	private static String pedirDniEmpleado() {

		System.out.println("Introduce el dni del empleado:");
		String dni = input.nextLine();

		return dni;

	}

	public static LocalDate pedirFechaFinContratoEmpleado() {

		System.out.println("Introduce la fecha de finalizaci�n del contrato: (a�o-mes-d�a, en formato YYYY-MM-DD)");
		String fecha = input.nextLine();

		return LocalDate.parse(fecha);

	}

	public static boolean introducirMasDatos(String dato) {

		char opcion; 
		
		do {
			
			System.out.println("�Quieres introducir otro " + dato + " ? S/N");
			opcion = input.nextLine().toLowerCase().charAt(0);	
			
			if (opcion != 's' && opcion != 'n') {
				System.out.println("La opci�n '" + opcion + "' no es v�lida. S/N");
			}
			
		} while (opcion != 's' && opcion != 'n');

		if (opcion == 'n') {
			return false;
		}

		return true;

	}

	public static boolean hayMonumentos() {

		if (zaragoza.getListaMonumentos().size() == 0) {
			System.out.println(
					"- Actualmente no hay monumentos dados de alta. Primero tienes que introducir monumentos -");
			return false;
		}

		return true;

	}

	public static boolean hayEmpleados() {

		if (zaragoza.getListaEmpleados().size() == 0) {
			System.out
					.println("- Actualmente no hay empleados dados de alta. Primero tienes que introducir empleados -");
			return false;
		}

		return true;

	}

	public static boolean hayMonumentosYEmpleados() {

		if (zaragoza.getListaEmpleados().size() == 0 && zaragoza.getListaMonumentos().size() == 0) {
			System.out.println(
					"- Actualmente no hay empleados ni monumentos dados de alta. Primero tienes que introducirlos -");
			return false;
		}

		return true;

	}

	public static void mostrarMensajeBuscarEmpleado(String dni, Empleado empleado) {

		if (empleado == null) {
			System.out.println("Empleado con el dni '" + dni + "' no existe");
		} else {
			System.out.println(empleado);
		}

	}

	public static void mostrarMensajeBuscarMonumento(String codigo, Monumento monumento) {

		if (monumento == null) {
			System.out.println("Monumento con el c�digo '" + codigo + "' no existe");
		} else {
			System.out.println(monumento);
		}

	}

	public static void mostrarMensajeEliminarEmpleado(String dni, boolean eliminado) {

		if (eliminado) {
			System.out.println("Empleado con el dni '" + dni + "' eliminado");
		} else {
			System.out.println("Empleado con el dni '" + dni + "' no existe");
		}

	}

	public static void mostrarMensajeEliminarMonumento(String codigo, boolean eliminado) {

		if (eliminado) {
			System.out.println("Monumento con el c�digo '" + codigo + "' eliminado");
		} else {
			System.out.println("Monumento con el c�digo '" + codigo + "' no existe");
		}

	}

	public static void terminarPrograma() {

		System.out.println("- Programa terminado -");
		input.close();
		System.exit(0);

	}

}
