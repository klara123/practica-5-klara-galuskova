package clases;

import java.util.ArrayList;

/**
 * Clase Monumento que contiene constructores necesarios para instanciar los
 * objetos de tipo Monumento. Contiene m�todos getters y setters, y el m�todo
 * toString().
 * 
 * @author Klara Galuskova, 1� DAW
 */
public class Monumento {

	// atributos
	private String nombre;
	private String codigoMonumento;
	private String estilo;
	private double precioEntrada;
	private ArrayList<Empleado> listaEmpleadosMonumento;
	private Empleado responsable;

	// constructores
	public Monumento() {
		this.nombre = "";
		this.codigoMonumento = "";
		this.estilo = "";
		this.precioEntrada = 0.0;
		this.listaEmpleadosMonumento = new ArrayList<Empleado>();
	}

	public Monumento(String nombre, String codigoMonumento, String estilo, double precioEntrada) {
		this.nombre = nombre;
		this.codigoMonumento = codigoMonumento;
		this.estilo = estilo;
		this.precioEntrada = precioEntrada;
		this.listaEmpleadosMonumento = new ArrayList<Empleado>();
	}

	// getters y setters
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCodigoMonumento() {
		return codigoMonumento;
	}

	public void setCodigoMonumento(String codigoMonumento) {
		this.codigoMonumento = codigoMonumento;
	}

	public String getEstilo() {
		return estilo;
	}

	public void setEstilo(String estilo) {
		this.estilo = estilo;
	}

	public double getPrecioEntrada() {
		return precioEntrada;
	}

	public void setPrecioEntrada(double precioEntrada) {
		this.precioEntrada = precioEntrada;
	}

	public ArrayList<Empleado> getListaEmpleadosMonumento() {
		return listaEmpleadosMonumento;
	}

	public void setListaEmpleadosMonumento(ArrayList<Empleado> listaEmpleadosMonumento) {
		this.listaEmpleadosMonumento = listaEmpleadosMonumento;
	}

	public Empleado getResponsable() {
		return responsable;
	}

	public void setResponsable(Empleado responsable) {
		this.responsable = responsable;
	}

	// toString
	@Override
	public String toString() {
		return "Monumento [nombre=" + nombre + ", codigoMonumento=" + codigoMonumento + ", estilo=" + estilo
				+ ", precioEntrada=" + precioEntrada + ", listaEmpleadosMonumento=" + listaEmpleadosMonumento
				+ ", responsable=" + responsable + "]";
	}

}
