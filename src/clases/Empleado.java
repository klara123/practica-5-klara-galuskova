package clases;

import java.time.LocalDate;

/**
 * Clase Empleado que contiene constructores necesarios para instanciar los
 * objetos de tipo Empleado. Contiene m�todos getters y setters, y el m�todo
 * toString().
 * 
 * @author Klara Galuskova, 1� DAW
 */
public class Empleado {

	// atributos
	private String nombre;
	private String apellidos;
	private String dni;
	private LocalDate fechaNacimiento;
	private LocalDate inicioContrato;
	private LocalDate finContrato;

	// constructores
	public Empleado() {
		this.nombre = "";
		this.apellidos = "";
		this.dni = "";
		this.inicioContrato = LocalDate.now();
	}

	public Empleado(String nombre, String apellidos, String dni, String fechaNacimiento, String inicioContrato) {
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.dni = dni;
		this.fechaNacimiento = LocalDate.parse(fechaNacimiento);
		this.inicioContrato = LocalDate.parse(inicioContrato);
	}

	// getters y setters
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public LocalDate getfechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(LocalDate fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public LocalDate getInicioContrato() {
		return inicioContrato;
	}

	public void setInicioContrato(LocalDate inicioContrato) {
		this.inicioContrato = inicioContrato;
	}

	public LocalDate getFinContrato() {
		return finContrato;
	}

	public void setFinContrato(LocalDate finContrato) {
		this.finContrato = finContrato;
	}

	// toString
	@Override
	public String toString() {
		return "Empleado [nombre=" + nombre + ", apellidos=" + apellidos + ", dni=" + dni + ", fechaNacimiento="
				+ fechaNacimiento + ", inicioContrato=" + inicioContrato + ", finContrato=" + finContrato + "]";
	}

}
