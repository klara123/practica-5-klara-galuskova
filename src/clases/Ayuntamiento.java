package clases;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Clase Ayuntamiento en la que se guardan ArrayList que contienen instancias de
 * las clases Empleado y Monumento. Contiene m�todos para gestionar las
 * instancias de esas clases.
 * 
 * @author Klara Galuskova, 1� DAW
 */
public class Ayuntamiento {

	/**
	 * {@value #IVA} Constante para guardar el valor del IVA que se usa en los
	 * m�todos de la clase Ayuntamiento.
	 */
	static final double IVA = 0.21;

	private ArrayList<Empleado> listaEmpleados;
	private ArrayList<Monumento> listaMonumentos;

	/**
	 * Constructor sin par�metros para crear la instancia de la clase Ayuntamiento.
	 * Inicializa los ArrayList que contendr�n las instancias de las clases Empleado
	 * y Monumento.
	 */
	public Ayuntamiento() {
		this.listaEmpleados = new ArrayList<Empleado>();
		this.listaMonumentos = new ArrayList<Monumento>();
	}

	/**
	 * Getter de listaEmpleados de tipo {@code ArrayList<Empleado>}
	 * 
	 * @return listaEmpleados
	 */
	public ArrayList<Empleado> getListaEmpleados() {
		return listaEmpleados;
	}

	/**
	 * Setter de listaEmpleados de tipo {@code ArrayList<Empleado>}
	 * 
	 * @param listaEmpleados lista de empleados dados de alta
	 */
	public void setListaEmpleados(ArrayList<Empleado> listaEmpleados) {
		this.listaEmpleados = listaEmpleados;
	}

	/**
	 * Getter del atributo getListaMonumentos de tipo {@code ArrayList<Monumento>}
	 * 
	 * @return getListaMonumentos
	 */
	public ArrayList<Monumento> getListaMonumentos() {
		return listaMonumentos;
	}

	/**
	 * Setter de listaMonumentos de tipo {@code ArrayList<Monumento>}
	 * 
	 * @param listaMonumentos lista de monumentos dados de alta
	 */
	public void setListaMonumentos(ArrayList<Monumento> listaMonumentos) {
		this.listaMonumentos = listaMonumentos;
	}

	/**
	 * M�todo para dar de alta a un nuevo monumento. Crea una nueva instancia de la
	 * clase Monumento mediante un constructor que usa los valores pasados por
	 * par�metro. Mediante la llamada al m�todo existeMonumento(String codigo)
	 * comprueba si la instancia con este par�metro ya existe, y as� evita objetos
	 * con valores duplicados. A�ade la instancia creada al ArrayList
	 * listaMonumentos.
	 * 
	 * @param nombre        nombre del monumento
	 * @param codigo        c�digo del monumento (tiene que ser �nico para cada
	 *                      monumento)
	 * @param estilo        estilo del monumento
	 * @param precioEntrada precio de las entradas al monumento
	 */
	public void altaMonumento(String nombre, String codigo, String estilo, double precioEntrada) {

		if (!existeMonumento(codigo)) {
			Monumento nuevoMonumento = new Monumento(nombre, codigo, estilo, precioEntrada);
			listaMonumentos.add(nuevoMonumento);
		} else {
			System.out.println("\nMonumento no se puede dar de alta - c�digo '" + codigo + "' duplicado");
		}

	}

	/**
	 * M�todo que recibe el c�digo del monumento cuya existencia queremos comprobar,
	 * y devuelve un boolean true/false seg�n si existe el monumento con el mismo
	 * c�digo o no.
	 * 
	 * @param codigo c�digo del monumento
	 * @return true si el monumento con el c�digo existe, y false si no existe
	 */
	public boolean existeMonumento(String codigo) {

		for (Monumento monumento : listaMonumentos) {
			if (monumento != null && monumento.getCodigoMonumento().equals(codigo)) {
				return true;
			}
		}

		return false;

	}

	/**
	 * M�todo que no recibe par�metros y que no devuelve nada. Lista los monumentos
	 * almacenados en la lista listaMonumentos. Si no hay monumentos almacenados,
	 * muestra un mensaje por consola.
	 */
	public void listarMonumentos() {

		Monumento monumento;

		for (int i = 0; i < listaMonumentos.size(); i++) {

			monumento = listaMonumentos.get(i);
			System.out.println((i + 1) + "/" + listaMonumentos.size() + " " + monumento);

		}

		if (listaMonumentos.size() == 0) {
			System.out.println("- No hay monumentos almacenados en el sistema gestor -");
		}

	}

	/**
	 * M�todo que recibe el c�digo del monumento que queremos encontrar, y devuelve
	 * el objeto de tipo Monumento. Si no encuentra el monumento con este c�digo,
	 * devuelve null.
	 * 
	 * @param codigo el c�digo del monumento
	 * @return monumento si encuentra el monumento con el c�digo, null si no lo
	 *         encuentra
	 */
	public Monumento buscarMonumento(String codigo) {

		for (Monumento monumento : listaMonumentos) {

			if (monumento != null && monumento.getCodigoMonumento().equals(codigo)) {
				return monumento;
			}

		}

		return null;

	}

	/**
	 * M�todo que elimina el monumento cuyo c�digo hemos pasado por par�metro.
	 * Devuelve un boolean true/false seg�n si ha encontrado y eliminado el
	 * monumento o no.
	 * 
	 * @param codigo c�digo del monumento
	 * @return true si ha eliminado el monumento, false si no ha encontrado el
	 *         monumento
	 */
	public boolean eliminarMonumento(String codigo) {

		Iterator<Monumento> iterator = listaMonumentos.iterator();
		Monumento monumentoActual;

		while (iterator.hasNext()) {
			monumentoActual = iterator.next();

			if (monumentoActual.getCodigoMonumento().equals(codigo)) {
				iterator.remove();
				return true;
			}

		}

		return false;

	}

	/**
	 * M�todo que lista los monumentos con el estilo recibido por par�metro. No es
	 * case sensitive y muestra tambi�n los monumentos que contienen la palabra
	 * pasada por par�metro (por ejemplo puedo introducir 'rom' en vez de 'rom�nico'
	 * para listar los monumentos). Si no existe el monumento con este estilo,
	 * muestra un mensaje por consola.
	 * 
	 * @param estilo estilo del monumento
	 */
	public void listarMonumentosPorEstilo(String estilo) {

		System.out.println("Monumentos con el estilo '" + estilo + "':");

		int counter = 0;

		String aux = estilo;
		estilo = estilo.toLowerCase();

		for (Monumento monumento : listaMonumentos) {

			if (monumento != null && monumento.getEstilo().toLowerCase().contains(estilo)) {
				counter++;
				System.out.println(counter + " - " + monumento);
			}

		}

		if (counter == 0) {
			System.out.println("- Monumento con el estilo '" + aux + "' no encontrado -");
		}

	}

	/**
	 * M�todo para dar de alta a un nuevo empleado. Crea una nueva instancia de la
	 * clase Empleado mediante un constructor que usa los valores pasados por
	 * par�metro. Mediante la llamada al m�todo existeEmpleado(String dni) comprueba
	 * si la instancia con este par�metro ya existe, y as� evita objetos con valores
	 * duplicados. A�ade la instancia creada al ArrayList listaEmpleados.
	 * 
	 * @param nombre          nombre del empleado
	 * @param apellidos       apellidos del empleado
	 * @param dni             dni del empleado (tiene que ser �nico para cada
	 *                        empleado)
	 * @param fechaNacimiento fecha de nacimiento del empleado
	 * @param inicioContrato  fecha de inicio de contrato del empleado
	 */
	public void altaEmpleado(String nombre, String apellidos, String dni, String fechaNacimiento,
			String inicioContrato) {

		if (!existeEmpleado(dni)) {
			Empleado nuevoEmpleado = new Empleado(nombre, apellidos, dni, fechaNacimiento, inicioContrato);
			listaEmpleados.add(nuevoEmpleado);
		} else {
			System.out.println("\nEmpleado no se puede dar de alta - dni '" + dni + "' duplicado");
		}

	}

	/**
	 * M�todo que recibe el DNI del empleado cuya existencia queremos comprobar, y
	 * devuelve un boolean true/false seg�n si existe el empleado con el mismo DNI o
	 * no.
	 * 
	 * @param dni DNI del empleado
	 * @return true si el empleado con el DNI existe, y false si no existe
	 */
	public boolean existeEmpleado(String dni) {

		for (Empleado empleado : listaEmpleados) {
			if (empleado != null && empleado.getDni().equals(dni)) {
				return true;
			}
		}

		return false;

	}

	/**
	 * * M�todo que no recibe par�metros y que no devuelve nada. Lista los empleados
	 * almacenados en la lista listaEmpleados. Si no hay empleados almacenados,
	 * muestra un mensaje por consola.
	 */
	public void listarEmpleados() {

		Empleado empleado;

		for (int i = 0; i < listaEmpleados.size(); i++) {

			empleado = listaEmpleados.get(i);
			System.out.println((i + 1) + "/" + listaEmpleados.size() + " " + empleado);

		}

		if (listaEmpleados.size() == 0) {
			System.out.println("- No hay empleados almacenados en el sistema gestor -");
		}

	}

	/**
	 * M�todo que recibe el DNI del empleado que queremos encontrar, y devuelve el
	 * objeto de tipo Empleado. Si no encuentra el empleado con este DNI, devuelve
	 * null.
	 * 
	 * @param dni DNI del empleado
	 * @return empleado si encuentra el empleado con el c�digo, null si no lo
	 *         encuentra
	 */
	public Empleado buscarEmpleado(String dni) {

		String dniActual;

		for (Empleado empleado : listaEmpleados) {

			if (empleado != null) {
				dniActual = empleado.getDni();

				if (dniActual.equals(dni)) {
					return empleado;
				}

			}

		}

		return null;

	}

	/**
	 * M�todo que elimina el empleado cuyo DNI hemos pasado por par�metro. Devuelve
	 * un boolean true/false seg�n si ha encontrado y eliminado el empleado o no.
	 * 
	 * @param dni DNI del empleado
	 * @return true si ha eliminado el empleado, false si no ha encontrado el
	 *         empleado
	 */
	public boolean eliminarEmpleado(String dni) {

		Iterator<Empleado> iterator = listaEmpleados.iterator();
		Empleado empleadoActual;

		while (iterator.hasNext()) {
			empleadoActual = iterator.next();

			if (empleadoActual.getDni().equals(dni)) {
				iterator.remove();
				return true;
			}

		}

		return false;

	}

	/**
	 * M�todo que asigna un empleado a un monumento. Llama a los m�todos
	 * buscarEmpleado(String dni) y buscarMonumento(String codigo) para encontrar
	 * los dos objetos. Si alguno de ellos no existe, nos muestra un mensaje por
	 * consola. Una vez comprobado que existe el empleado y el monumento con los
	 * atributos pasados por par�metro, llama al m�todo estaContratado(String dni)
	 * para comprobar que a�n no ha finalizado el contrato del empleado. Una vez
	 * hecha esta comprobaci�n, llama al m�todo trabajaEnMonumento(String dni,
	 * {@code ArrayList<Empleado>} empleadosMonumento), que comprueba si el mismo
	 * empleado ya est� asignado al monumento o no, y as� evitar empleados
	 * duplicados en el mismo monumento. Despu�s de hacer esta �ltima comprobaci�n,
	 * a�ade el empleado al ArrayList listaEmpleadosMonumento del monumento.
	 * 
	 * @param codigo c�digo del monumento
	 * @param dni    DNI del empleado que queremos asignar al monumento
	 */
	public void asignarEmpleadoMonumento(String codigo, String dni) {

		Monumento monumento = buscarMonumento(codigo);

		if (monumento != null) {

			ArrayList<Empleado> empleadosMonumento = monumento.getListaEmpleadosMonumento();
			Empleado empleadoNuevo = buscarEmpleado(dni);

			if (empleadoNuevo != null && estaContratado(dni)) {

				if (!trabajaEnMonumento(dni, empleadosMonumento)) {
					empleadosMonumento.add(empleadoNuevo);
					System.out.println("Empleado con el dni '" + dni + "' asignado al monumento '" + codigo + "'");
				} else {
					System.out.println("Empleado no se puede asignar al monumento - posible duplicidad");
				}

			} else {

				if (empleadoNuevo == null) {
					System.out.println("Empleado con el dni '" + dni + "' no existe");
				} else {
					System.out.println("Empleado con el dni '" + dni
							+ "' no se puede asignar al monumento. Su contrato ha terminado el "
							+ empleadoNuevo.getFinContrato());
				}

			}

		} else {
			System.out.println("Monumento con el c�digo '" + codigo + "' no existe");
		}

	}

	/**
	 * M�todo que lista todos los monumentos a los que est� asignado el empleado con
	 * el DNI pasado por par�metro. Hace una llamada al m�todo buscarEmpleado(String
	 * dni) para localizar al empleado. Si el empleado con el DNI no existe, muestra
	 * un mensaje por consola. En el caso contrario lista los monumentos a los que
	 * est� asignado.
	 * 
	 * @param dni DNI del empleado
	 */
	public void listarMonumentosEmpleado(String dni) {

		Empleado empleado = buscarEmpleado(dni);

		if (empleado != null) {

			System.out.println("Monumentos del empleado '" + dni + "':");

			int counter = 0;

			for (Monumento monumento : listaMonumentos) {

				if (monumento != null && monumento.getListaEmpleadosMonumento().contains(empleado)) {
					counter++;
					System.out.println(counter + " - " + monumento);
				}

			}

			if (counter == 0) {
				System.out.println("- Empleado con el dni '" + dni + "' no trabaja en ning�n monumento -");
			}

		} else {
			System.out.println("Empleado con el dni '" + dni + "' no existe");
		}

	}

	/**
	 * M�todo que comprueba si el empleado cuyo DNI hemos pasado por par�metro est�
	 * contratado o no. Devuelve true si la fecha de finalizaci�n de su contrato de
	 * este empleado es null, o mayor que la fecha actual. En el caso contrario
	 * devuelve false. Hace una llamada al m�todo buscarEmpleado(String dni), para
	 * localizar el empleado.
	 * 
	 * @param dni DNI del empleado
	 * @return true si el empleado est� contratado, false si su contrato ha
	 *         terminado
	 */
	public boolean estaContratado(String dni) {

		LocalDate fechaActual = LocalDate.now();
		Empleado empleado = buscarEmpleado(dni);

		if (empleado != null) {

			LocalDate finContrato = empleado.getFinContrato();

			if (finContrato == null || finContrato.isAfter(fechaActual)) {
				return true;
			}

		}

		return false;

	}

	/**
	 * M�todo que recibe el DNI del empleado nuevo, y el ArrayList de empleados del
	 * monumento actual, y comprueba si el empleado se encuentra en el ArrayList o
	 * no, para no darlo de alta m�ltiples veces en el mismo monumento. Si se
	 * encuentra en el ArrayList del monumento, devuelve true, en el caso contrario
	 * devuelve false.
	 * 
	 * @param dni       DNI del empleado
	 * @param empleados lista de empleados del monumento actual
	 * @return true si el monumento se encuentra en la lista de empleados del
	 *         monumento, false si no est� en esta lista
	 */
	public boolean trabajaEnMonumento(String dni, ArrayList<Empleado> empleados) {

		for (Empleado empleadoActual : empleados) {
			if (empleadoActual != null && empleadoActual.getDni().equals(dni)) {
				return true;
			}
		}

		return false;

	}

	/**
	 * M�todo para asignar el responsable al monumento. Elige al responsable
	 * autom�ticamente entre los empleados del monumento seg�n quien lleva m�s
	 * tiempo contratado (compara fechas de inicio de contrato). Para encontrar al
	 * responsable y para comparar las fechas llama al m�todo
	 * empleadoMayorAntiguedad({@code ArrayList<Empleado>} empleados). En caso de
	 * que hay varios empleados con la misma fecha de inicio de contrato, este
	 * m�todo elige al m�s mayor (compara fechas de nacimiento). Solo recibe el
	 * c�digo del monumento cuyo responsable queremos asignar. Tambi�n hace una
	 * llamada al m�todo buscarMonumento(String codigo) para encontrar al monumento.
	 * En casos de que no se puede asignar un responsable (si el monumento no
	 * existe, si el monumento no tiene empleados asignados, o si los empleados
	 * asignados tienen contratos finalizados), el m�todo muestra el mensaje
	 * correspondiente por consola. En caso de que encuentra al responsable, asigna
	 * el responsable al monumento correspondiente.
	 * 
	 * @param codigo c�digo del monumento
	 */
	public void asignarResponsableMonumento(String codigo) {

		Monumento monumento = buscarMonumento(codigo);

		if (monumento != null) {

			ArrayList<Empleado> empleados = monumento.getListaEmpleadosMonumento();

			if (empleados.size() == 0) {
				System.out
						.println("- No se puede asignar responsable. El monumento '" + codigo + "' no tiene empleados");
			} else {

				Empleado responsable = empleadoMayorAntiguedad(empleados);

				if (responsable == null) {
					System.out.println(
							"- No se puede asignar responsable. Los contratos de los empleados de este monumento est�n terminados -");
				} else {
					monumento.setResponsable(responsable);
					System.out.println("Responsable del monumento '" + codigo + "': " + monumento.getResponsable());
				}

			}

		} else {
			System.out.println("El monumento con el c�digo '" + codigo + "' no existe.");
		}

	}

	/**
	 * M�todo para asignar el empleado cuyo DNI recibe por par�metro como
	 * responsable del monumento cuyo c�digo recibe por par�metro. Llama al m�todo
	 * buscarEmpleado(String dni) y buscarMonumento(String codigo) para encontrar el
	 * monumento y el empleado. Si no existe el monumento o el empleado, muestra un
	 * mensaje por consola. Antes de asignar el empleado como responsable del
	 * monumento, llama al m�todo estaContratado(String dni), para comprobar si el
	 * contrato de este empleado es v�lido. Despu�s lo asigna como responsable del
	 * monumento. Tambi�n llama al m�todo trabajaEnMonumento(String dni,
	 * {@code ArrayList<Empleado>} empleados), para comprobar si el responsable ya
	 * est� incluido en la lista de empleados de este monumento, y si no es el caso
	 * lo a�ade al ArrayList listaEmpleadosMonumento de este monumento.
	 * 
	 * @param codigo c�digo del monumento
	 * @param dni    DNI del empleado
	 */
	public void asignarResponsableMonumento(String codigo, String dni) {

		Monumento monumento = buscarMonumento(codigo);

		if (monumento != null) {

			ArrayList<Empleado> empleados = monumento.getListaEmpleadosMonumento();
			Empleado responsable = buscarEmpleado(dni);

			if (responsable != null && estaContratado(dni)) {

				monumento.setResponsable(responsable);
				System.out.println("Responsable del monumento '" + codigo + "': " + monumento.getResponsable());

				if (!trabajaEnMonumento(dni, empleados)) {
					empleados.add(responsable);
				}

			} else {

				if (responsable == null) {
					System.out.println("El empleado con el dni '" + dni + "' no existe");
				} else {
					System.out.println("Empleado con el dni '" + dni
							+ "' no se puede asignar como responsable. Su contrato ha terminado el "
							+ responsable.getFinContrato());
				}

			}

		} else {
			System.out.println("El monumento con el c�digo '" + codigo + "' no existe");
		}

	}

	/**
	 * M�todo que asigna la fecha de finalizaci�n de contrato de un empleado. Le
	 * pasamos por par�metro el DNI del empleado, y la fecha de finalizaci�n de su
	 * contrato. El m�todo hace una llamada al m�todo buscarEmpleado(String dni),
	 * para encontrar al empleado.
	 * 
	 * @param dni      DNI del empleado
	 * @param fechaFin fecha de finalizaci�n de contrato
	 */
	public void anadirFinContratoEmpleado(String dni, LocalDate fechaFin) {

		Empleado empleado = buscarEmpleado(dni);

		if (empleado != null) {

			empleado.setFinContrato(fechaFin);
			System.out.println("Fecha fin de contrato del empleado '" + dni + "': " + empleado.getFinContrato());

		} else {
			System.out.println("El empleado con el dni '" + dni + "' no encontrado");
		}

	}

	/**
	 * M�todo que recibe el c�digo de un monumento, y elimina a todos sus empleados
	 * cuya fecha de finalizaci�n de contrato es menor que la fecha actual (elimina
	 * los empleados despedidos). Hace una llamada al m�todo buscarMonumento(String
	 * codigo) para encontrar al monumento. Para determinar si el empleado est�
	 * contratado o no, llama al m�todo estaContratado(String dni) que devuelve
	 * false si el contrato del empleado ha finalizado. Si el contrato del empleado
	 * est� finalizado, lo elimina de la lista de empleados del monumento. El m�todo
	 * muestra un mensaje por consola cada vez que elimina a un empleado. Tambi�n
	 * muestra mensajes por consola si no elimina a ning�n empleado, si el monumento
	 * no tiene ning�n empleado asignado, o si el monumento con el c�digo recibido
	 * no existe.
	 * 
	 * @param codigo c�digo del monumento
	 */
	public void bajaEmpleados(String codigo) {

		Monumento monumento = buscarMonumento(codigo);

		if (monumento != null) {

			int counter = 0;

			ArrayList<Empleado> empleadosMonumentoActual = monumento.getListaEmpleadosMonumento();

			if (empleadosMonumentoActual.size() > 0) {

				Iterator<Empleado> iterator = empleadosMonumentoActual.iterator();

				while (iterator.hasNext()) {

					Empleado empleadoActual = iterator.next();

					String dniEmpleadoActual = empleadoActual.getDni();
					LocalDate finContrato = empleadoActual.getFinContrato();

					if (!estaContratado(dniEmpleadoActual)) {
						iterator.remove();
						counter++;
						System.out.println(counter + " - Empleado '" + dniEmpleadoActual
								+ "' cuyo contrato ha terminado el " + finContrato + " eliminado");
					}

				}

				if (counter == 0) {
					System.out.println(
							"No se ha eliminado ning�n empleado - todos los empleados tienen los contratos v�lidos");
				}

			} else {
				System.out.println("El monumento '" + codigo + "' no tiene empleados asignados");
			}

		} else {

			System.out.println("El monumento con el c�digo '" + codigo + "' no encontrado");

		}

	}

	/**
	 * 
	 * M�todo que recorre la lista de empleados asignados a un monumento, y elige el
	 * empleado cuyo contrato ha empezado antes. Hace uso del m�todo
	 * obtenerFechaMayorAntiguedad({@code ArrayList<Empleado>} empleados) que
	 * devuelve la fecha de contrato del empleado que ha sido contratado antes.
	 * Desp�s recorre la lista de empleados del monumento, y guarda al empleado con
	 * esta fecha. En caso de que hay varios empleados cuyo contrato e empieza en
	 * esta fecha, se da el puesto de responsable al empleado que ha nacido antes.
	 * El m�todo devuelve null si no encuentra al responsable, y en caso contrario
	 * devuelve el objeto Empleado que cumple las condiciones anteriores.
	 * 
	 * @param empleados lista de empleados asignados a un monumento
	 * @return devuelve null si no elige al responsable, si lo elige devuelve el
	 *         objeto responsable de la clase Empleado
	 */
	public Empleado empleadoMayorAntiguedad(ArrayList<Empleado> empleados) {

		LocalDate fechaMayorAntiguedad = obtenerFechaMayorAntiguedad(empleados);
		Empleado responsable = null;

		if (fechaMayorAntiguedad != null) {
			for (Empleado empleadoActual : empleados) {

				if (empleadoActual != null && estaContratado(empleadoActual.getDni())) {
					LocalDate inicioContrato = empleadoActual.getInicioContrato();

					if (inicioContrato.equals(fechaMayorAntiguedad)) {
						if (responsable == null) {
							responsable = empleadoActual;
						} else if (responsable.getfechaNacimiento().isAfter(empleadoActual.getfechaNacimiento())) {
							responsable = empleadoActual;
						}
					}

				}

			}

		}

		return responsable;

	}

	/**
	 * M�todo para obtener la fecha de inicio de contrato del empleado con mayor
	 * antig�edad en un monumento concreto. Recibe el ArrayList de los empleados de
	 * este monumento. Devuelve la fecha de inicio de contrato del empleado con m�s
	 * antig�edad en el monumento. Si no la encuentra (por ejemplo en caso de que el
	 * monumento a�n no tiene empleados asignados), devuelve null.
	 * 
	 * @param empleados lista de empleados asignados a un monumento
	 * @return fecha de inicio de contrato del empleado con mayor antig�edad, null
	 *         en caso de que no hay empleados asignados al monumento
	 */
	public LocalDate obtenerFechaMayorAntiguedad(ArrayList<Empleado> empleados) {

		LocalDate fechaMayorAntiguedad = null;

		for (Empleado empleadoActual : empleados) {

			if (empleadoActual != null) {
				LocalDate inicioContrato = empleadoActual.getInicioContrato();

				if (fechaMayorAntiguedad == null) {
					fechaMayorAntiguedad = inicioContrato;
				} else if (inicioContrato.isBefore(fechaMayorAntiguedad)) {
					fechaMayorAntiguedad = inicioContrato;
				}

			}

		}

		return fechaMayorAntiguedad;

	}

	/**
	 * M�todo que suma IVA al precio del monumento cuyo c�digo recibe por par�metro,
	 * y le asigna al monumento el nuevo precio. Tambi�n muestra un mensaje por
	 * consola con el nuevo precio. Llama al m�todo buscarMonumento(String codigo),
	 * para localizar el monumento cuyo precio queremos actualizar.
	 * 
	 * @param codigo c�digo del monumento
	 */
	public void sumarIvaAlPrecio(String codigo) {

		Monumento monumento = buscarMonumento(codigo);

		if (monumento != null) {

			double precioConIva = monumento.getPrecioEntrada();
			precioConIva += (precioConIva * IVA);
			monumento.setPrecioEntrada(precioConIva);

			System.out.println("Precio con IVA: " + precioConIva + " �");

		} else {

			System.out.println("Monumento con el c�digo '" + codigo + "' no encontrado");

		}

	}

	// toString
	@Override
	public String toString() {
		return "Ayuntamiento [listaEmpleados=" + listaEmpleados + ", listaMonumentos=" + listaMonumentos + "]";
	}

}
